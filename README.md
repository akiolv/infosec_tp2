# <img src="app/src/main/res/mipmap-xxxhdpi/cs_launcher_round.png" height="60"> TP2 - Asynchronous tasks and Sensors

This Android application is for educational purposes in order to master the concepts of authentication and online data recovery. For this purpose, this application uses Flickr and Google maps (to display the location of the phone).

## Screenshots

<img src="screenshot/Screenshot_20210514-102717_One%20UI%20Home.jpg" alt="drawing" height="400"/>
<img src="screenshot/Screenshot_20210514-102724_InfoSec%20TP2.jpg" alt="drawing" height="400"/>
<img src="screenshot/Screenshot_20210514-102826_InfoSec%20TP2.jpg" alt="drawing" height="400"/>
<img src="screenshot/Screenshot_20210514-102743_InfoSec%20TP2.jpg" alt="drawing" height="400"/>
<img src="screenshot/Screenshot_20210514-102737_InfoSec%20TP2.jpg" alt="drawing" height="400"/>
<img src="screenshot/Screenshot_20210514-102751_InfoSec%20TP2.jpg" alt="drawing" height="400"/>
<img src="screenshot/Screenshot_20210514-102812_InfoSec%20TP2.jpg" alt="drawing" height="400"/>
<img src="screenshot/Screenshot_20210514-102819_InfoSec%20TP2.jpg" alt="drawing" height="400"/>


## Build and utilisation

To build the application, you will need a Google API key to put in the local.properties file in order to make the last feature available (Search by location).  

However you can install it with the apk/appBundle provided in the project release folder with the latest version of the application.

## Permission

```
<uses-permission android:name="android.permission.INTERNET"/>
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
```
