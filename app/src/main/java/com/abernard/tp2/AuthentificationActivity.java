package com.abernard.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.abernard.tp2.data.Authentification;

import org.w3c.dom.Text;

public class AuthentificationActivity extends AppCompatActivity {

    private CardView loginButton;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentification);

        this.loginButton = findViewById(R.id.loginButton);
        toolbar = (Toolbar) findViewById(R.id.topBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EditText usernameInput = (EditText) findViewById(R.id.editTextUsername);
        EditText passwordInput = (EditText) findViewById(R.id.editTextPassword);

        TextView resultOfConnection = (TextView) findViewById(R.id.resultOfConnection);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultOfConnection.setText("Connection...");
                Runnable uiRunnable = () -> {
                    if(Authentification.getAuth().isConnected()){
                        resultOfConnection.setText("Welcome " + Authentification.getAuth().getUser().getUsername() + "!");
                    } else {
                        resultOfConnection.setText("Authentification failed");
                    }
                };
                Runnable runnable = () -> {
                    Authentification.getAuth().SignIn(usernameInput.getText().toString(), passwordInput.getText().toString());
                    runOnUiThread(uiRunnable);
                };
                new Thread(runnable).start();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_menu, menu);
        menu.setGroupVisible(R.id.actionButtons, false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        switch (id){
            case R.id.setting:
                Toast.makeText(this, "Settings Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.about:
                Toast.makeText(this, "About Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.connexion:
            case R.id.connexionTab:
                Intent connexionActivity = new Intent(getApplicationContext(), AuthentificationActivity.class);
                startActivity(connexionActivity);
                break;
            case R.id.getAnImageTab:
                Intent flickrImageActivity = new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(flickrImageActivity);
                break;
            case R.id.getAnImageTabWithPos:
                Intent flickrWithPosActivity = new Intent(getApplicationContext(), SearchWithPosActivity.class);
                startActivity(flickrWithPosActivity);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}