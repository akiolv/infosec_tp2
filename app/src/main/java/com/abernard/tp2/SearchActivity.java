package com.abernard.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.abernard.tp2.data.Authentification;

public class SearchActivity extends AppCompatActivity {

    private CardView searchbutton;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_input_for_search);

        this.searchbutton = findViewById(R.id.searchbutton);
        toolbar = (Toolbar) findViewById(R.id.topBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EditText searchInput = (EditText) findViewById(R.id.searchText);

        searchbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent connexionActivity = new Intent(getApplicationContext(), FlickrImageActivity.class);
                connexionActivity.putExtra("search_string", searchInput.getText().toString());
                startActivity(connexionActivity);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_menu, menu);
        menu.setGroupVisible(R.id.actionButtons, true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        switch (id){
            case R.id.setting:
                Toast.makeText(this, "Settings Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.about:
                Toast.makeText(this, "About Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.connexion:
            case R.id.connexionTab:
                Intent connexionActivity = new Intent(getApplicationContext(), AuthentificationActivity.class);
                startActivity(connexionActivity);
                break;
            case R.id.getAnImageTab:
                Intent flickrImageActivity = new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(flickrImageActivity);
                break;
            case R.id.getAnImageTabWithPos:
                Intent flickrWithPosActivity = new Intent(getApplicationContext(), SearchWithPosActivity.class);
                startActivity(flickrWithPosActivity);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}