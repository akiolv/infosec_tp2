package com.abernard.tp2.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.abernard.tp2.R;
import com.abernard.tp2.data.FlickrImageListController;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.w3c.dom.Text;

import java.util.List;

public class FlickrImageListAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<FlickrImage> flickrImageList;

    ImageLoader imageLoader = FlickrImageListController.getController().getImageLoader();

    public FlickrImageListAdapter(Activity activity, List<FlickrImage> flickrImageList){
        this.activity = activity;
        this.flickrImageList = flickrImageList;
    }

    @Override
    public int getCount() {
        if(flickrImageList == null){
            return 0;
        }
        return flickrImageList.size();
    }

    @Override
    public Object getItem(int position) {
        return flickrImageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null)
            convertView = inflater.inflate(R.layout.list_row, null);
        if(imageLoader == null)
            imageLoader = FlickrImageListController.getController().getImageLoader();

        NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.thumbnail);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView author = (TextView) convertView.findViewById(R.id.author);
        TextView date = (TextView) convertView.findViewById(R.id.date);

        // Get the FlickrImage for the row
        FlickrImage flickrImage = flickrImageList.get(position);

        // set the thumbnail
        thumbNail.setImageUrl(flickrImage.getThumbnailUrl(), imageLoader);

        // title
        title.setText(flickrImage.getTitle());

        // author
        author.setText(flickrImage.getAuthor());

        // date
        date.setText(flickrImage.getDate());

        return convertView;
    }
}
