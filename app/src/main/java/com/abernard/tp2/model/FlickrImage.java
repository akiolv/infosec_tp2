package com.abernard.tp2.model;

public class FlickrImage {

    private String thumbnailUrl;
    private String title;
    private String author;
    private String date;

    public FlickrImage(){
    }

    public FlickrImage(String thumbnailUrl, String title, String author, String date){
        this.thumbnailUrl = thumbnailUrl;
        this.title = title;
        this.author = author;
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
