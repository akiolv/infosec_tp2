package com.abernard.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.abernard.tp2.data.Authentification;
import com.abernard.tp2.data.FlickerJSONData;
import com.abernard.tp2.model.FlickrImage;
import com.abernard.tp2.model.FlickrImageListAdapter;

import java.util.ArrayList;
import java.util.List;

public class FlickrImageActivity extends AppCompatActivity {

    Toolbar toolbar;
    private final String urlEarly = "https://www.flickr.com/services/feeds/photos_public.gne?tags=";
    private String urlTag;
    private final String urlEnding = "&format=json";
    private ArrayList<FlickrImage> flickrImageList = new ArrayList<FlickrImage>();
    private ListView listView;
    private FlickrImageListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_listview);

        Intent intent = getIntent();
        urlTag = intent.getStringExtra("search_string");

        if(urlTag == null || urlTag.isEmpty() || urlTag == "" || urlTag == " " || urlTag.length() > 20 || urlTag.contains(" ")){
            Toast.makeText(this, "Invalid search, remplaced by cats because their are cut", Toast.LENGTH_LONG).show();
            urlTag = "cats";
        }

        toolbar = (Toolbar) findViewById(R.id.topBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.flickrListView);

        TextView searchString = (TextView) findViewById(R.id.searchString);
        searchString.setText("Search: \"" + urlTag + '"');

        new FlickerJSONData(new FlickerJSONData.AsyncResponse(){
            @Override
            public void processFinish(List<FlickrImage> output) {
                flickrImageList.addAll(output);
                adapter.notifyDataSetChanged();
            }
        }).execute(urlEarly + urlTag + urlEnding, "1");

        adapter = new FlickrImageListAdapter(this, flickrImageList);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_menu, menu);
        menu.setGroupVisible(R.id.actionButtons, true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        switch (id){
            case R.id.setting:
                Toast.makeText(this, "Settings Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.about:
                Toast.makeText(this, "About Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.connexion:
            case R.id.connexionTab:
                Intent connexionActivity = new Intent(getApplicationContext(), AuthentificationActivity.class);
                startActivity(connexionActivity);
                break;
            case R.id.getAnImageTab:
                Intent flickrImageActivity = new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(flickrImageActivity);
                break;
            case R.id.getAnImageTabWithPos:
                Intent flickrWithPosActivity = new Intent(getApplicationContext(), SearchWithPosActivity.class);
                startActivity(flickrWithPosActivity);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
