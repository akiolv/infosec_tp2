package com.abernard.tp2.data;

import android.database.Observable;
import android.util.Log;
import android.widget.Toast;

import com.abernard.tp2.model.User;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.stream.Stream;

import javax.net.ssl.HttpsURLConnection;

public class Authentification {

    private static Authentification sAuth;
    private User user;
    private boolean connected;

    private Authentification(){
        this.user = new User(null);
        this.connected = false;
    }

    public static Authentification getAuth(){
        if(sAuth == null){
            sAuth = new Authentification();
        }
        return sAuth;
    }

    public void SignIn(String username, String password){
        System.out.println("Authentification of " + username);
        URL url = null;
        boolean con = false;
        try {
            // Setup the URL for the connection with username = bob et password = sympa
            url = new URL("https://httpbin.org/basic-auth/bob/sympa");

            // Connection with HTTPS to the URL with openConnection() method.
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();

            // Encoded username and password in Base64 to send it in the Header
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
            String authHeaderValue = "Basic " + new String(encodedAuth);

            // We send the header in Authorization property
            urlConnection.setRequestProperty("Authorization", authHeaderValue);

            // Setup the connection with GET (Output = false with GET)
            urlConnection.setDoOutput(false);
            urlConnection.setRequestMethod("GET");

            // Define a timeout
            urlConnection.setConnectTimeout(10000);

            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                System.out.println(username + " is connected");
                user.setUsername(username);
                con = true;
            } finally {
                urlConnection.disconnect();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        connected = con;
    }

    public boolean isConnected() {
        return connected;
    }

    public User getUser() {
        return user;
    }
}