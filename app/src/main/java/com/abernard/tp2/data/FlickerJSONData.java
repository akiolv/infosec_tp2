package com.abernard.tp2.data;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.abernard.tp2.model.FlickrImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FlickerJSONData extends AsyncTask<String, Void, JSONObject> {

    private String parseType;

    public FlickerJSONData(AsyncResponse delegate) {
        this.delegate = delegate;
    }

    public interface AsyncResponse {
        void processFinish(List<FlickrImage> output);
    }

    public AsyncResponse delegate = null;

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        System.out.println("[FlickerJSONData] Loading...");
    }

    @Override
    protected JSONObject doInBackground(String... params){

        parseType = params[1];
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        // Make http request
        try {
            // Get the url in the params of the .execute()
            URL url = new URL(params[0]);

            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            InputStream stream = connection.getInputStream();

            reader = new BufferedReader(new InputStreamReader(stream));

            StringBuffer buffer = new StringBuffer();
            String line = "";

            while ((line = reader.readLine()) != null) {
                buffer.append(line+"\n");
            }

            if(parseType == "1"){
                buffer.delete(0, 15);
                buffer.deleteCharAt(buffer.length() - 2);
            } else if (parseType == "2") {
                buffer.delete(0, 14);
            }

            return new JSONObject(buffer.toString());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject json) {

        ArrayList<FlickrImage> res = new ArrayList<FlickrImage>();
        try {
            if(parseType == "1"){
                if(json != null && json.getJSONArray("items") != null){

                    for(int i = 0; i < json.getJSONArray("items").length(); i++){
                        FlickrImage flickrImage = new FlickrImage();

                        // Set the information
                        flickrImage.setThumbnailUrl(json.getJSONArray("items").getJSONObject(i).getJSONObject("media").getString("m"));
                        flickrImage.setTitle(json.getJSONArray("items").getJSONObject(i).getString("title"));
                        flickrImage.setAuthor(json.getJSONArray("items").getJSONObject(i).getString("author"));
                        flickrImage.setDate(json.getJSONArray("items").getJSONObject(i).getString("published"));

                        res.add(flickrImage);
                    }
                }
            } else if (parseType == "2") {
                if(json != null && json.getJSONObject("photos") != null && json.getJSONObject("photos").getJSONArray("photo") != null){

                    for(int i = 0; i < json.getJSONObject("photos").getJSONArray("photo").length(); i++){
                        FlickrImage flickrImage = new FlickrImage();

                        // Set the information
                        flickrImage.setThumbnailUrl(json.getJSONObject("photos").getJSONArray("photo").getJSONObject(i).getString("url_m"));
                        flickrImage.setTitle(json.getJSONObject("photos").getJSONArray("photo").getJSONObject(i).getString("title"));
                        flickrImage.setAuthor(json.getJSONObject("photos").getJSONArray("photo").getJSONObject(i).getString("ownername"));
                        flickrImage.setDate(json.getJSONObject("photos").getJSONArray("photo").getJSONObject(i).getString("datetaken"));

                        res.add(flickrImage);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // return the results in the function defined by the user in the async task
        delegate.processFinish(res);
    }
}
