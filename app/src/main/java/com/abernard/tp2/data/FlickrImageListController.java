package com.abernard.tp2.data;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.abernard.tp2.FlickrImageActivity;
import com.abernard.tp2.utils.LruBitmapCache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class FlickrImageListController extends Application{

    // TAG is FlickrImageListController here
    public static final String TAG = FlickrImageListController.class.getSimpleName();

    private RequestQueue sRequestQueue;
    private ImageLoader sImageLoader;

    private static FlickrImageListController sController;

    @Override
    public void onCreate() {
        super.onCreate();
        sController = this;
    }

    // To avoid more than one process to get the Controller
    public static synchronized FlickrImageListController getController(){
        return sController;
    }

    public RequestQueue getRequestQueue(){
        if (sRequestQueue == null) {
            sRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return sRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if(sImageLoader == null){
            sImageLoader = new ImageLoader(this.sRequestQueue, new LruBitmapCache());
        }
        return this.sImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (sRequestQueue != null) {
            sRequestQueue.cancelAll(tag);
        }
    }
}
