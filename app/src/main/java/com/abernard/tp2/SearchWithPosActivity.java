package com.abernard.tp2;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import com.abernard.tp2.data.Authentification;
import com.abernard.tp2.data.FlickerJSONData;
import com.abernard.tp2.model.FlickrImage;
import com.abernard.tp2.model.FlickrImageListAdapter;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

public class SearchWithPosActivity extends AppCompatActivity implements OnMapReadyCallback {

    Toolbar toolbar;
    private final String urlEarly = new String("https://api.flickr.com/services/rest/?" +
            "method=flickr.photos.search" +
            "&license=4" +
            "&api_key=e07c644816990c8a766ef6a1df762219" +
            "&has_geo=1&lat=");
    private final String urlMiddle = "&lon=";
    private final String urlEnding = "&per_page=10&format=json&extras=date_upload,owner_name,views,media,url_m,date_taken";
    private String urlTag;
    private ArrayList<FlickrImage> flickrImageList = new ArrayList<FlickrImage>();
    private ListView listView;
    private FlickrImageListAdapter adapter;
    private GoogleMap mMap;
    private FlickerJSONData flickerJSONData;

    // Init location variable
    FusedLocationProviderClient fusedLocationProviderClient;
    private double latitude;
    private double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_search_with_position);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);

        toolbar = (Toolbar) findViewById(R.id.topBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.flickrListViewWithPos);

        // Init the location Provider
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        // Check Permission
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getLocation();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
        }

        TextView searchString = (TextView) findViewById(R.id.PosTextView);
        searchString.setText("Position:");

        flickerJSONData = new FlickerJSONData(new FlickerJSONData.AsyncResponse() {
            @Override
            public void processFinish(List<FlickrImage> output) {
                flickrImageList.addAll(output);
                adapter.notifyDataSetChanged();
            }
        });

        adapter = new FlickrImageListAdapter(this, flickrImageList);
        listView.setAdapter(adapter);
    }

    @SuppressLint("MissingPermission")
    private void getLocation() {
        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                Location location = task.getResult();
                if(location != null){
                    TextView searchString = (TextView) findViewById(R.id.PosTextView);
                    searchString.setText("Position: " + location.getLatitude() + ", " + location.getLongitude());
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    // Add a marker in Sydney and move the camera
                    LatLng device = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(device).title("You are here"));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(device, 10));
                    flickerJSONData.execute(urlEarly + location.getLatitude() + urlMiddle + location.getLongitude() + urlEnding, "2");
                } else {
                    TextView searchString = (TextView) findViewById(R.id.PosTextView);
                    searchString.setText("Position: -34°, 151°");
                    latitude = -34.0D;
                    longitude = 151.0D;

                    // Add a marker in Sydney and move the camera
                    LatLng sydney = new LatLng(-34, 151);
                    mMap.addMarker(new MarkerOptions().position(sydney).title("Sydney"));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 8));
                    flickerJSONData.execute(urlEarly + "-34" + urlMiddle + "151" + urlEnding, "2");
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 44:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    TextView searchString = (TextView) findViewById(R.id.PosTextView);
                    searchString.setText("Position: -34°, 151°");
                    latitude = -34.0D;
                    longitude = 151.0D;

                    // Add a marker in Sydney and move the camera
                    LatLng sydney = new LatLng(-34, 151);
                    mMap.addMarker(new MarkerOptions().position(sydney).title("Sydney"));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 8));
                    flickerJSONData.execute(urlEarly + "-34" + urlMiddle + "151" + urlEnding, "2");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setBuildingsEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(0.0, 0.0), 2.0f));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_menu, menu);
        menu.setGroupVisible(R.id.actionButtons, true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        switch (id){
            case R.id.setting:
                Toast.makeText(this, "Settings Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.about:
                Toast.makeText(this, "About Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.connexion:
            case R.id.connexionTab:
                Intent connexionActivity = new Intent(getApplicationContext(), AuthentificationActivity.class);
                startActivity(connexionActivity);
                break;
            case R.id.getAnImageTab:
                Intent flickrImageActivity = new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(flickrImageActivity);
                break;
            case R.id.getAnImageTabWithPos:
                Intent flickrWithPosActivity = new Intent(getApplicationContext(), SearchWithPosActivity.class);
                startActivity(flickrWithPosActivity);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}