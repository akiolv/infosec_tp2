package com.abernard.tp2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.abernard.tp2.data.FlickerJSONData;
import com.abernard.tp2.model.FlickrImage;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private CardView connexion;
    private CardView getAnImage;
    private CardView getAnImageWithPos;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.connexion = findViewById(R.id.connexion);
        this.getAnImage = findViewById(R.id.getAnImage);
        this.getAnImageWithPos = findViewById(R.id.getPositionImage);
        toolbar = (Toolbar) findViewById(R.id.topBar);
        setSupportActionBar(toolbar);

        getAnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent flickrImageActivity = new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(flickrImageActivity);
            }
        });

        connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent connexionActivity = new Intent(getApplicationContext(), AuthentificationActivity.class);
                startActivity(connexionActivity);
            }
        });

        getAnImageWithPos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent connexionActivity = new Intent(getApplicationContext(), SearchWithPosActivity.class);
                startActivity(connexionActivity);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_menu, menu);
        menu.setGroupVisible(R.id.actionButtons, true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        switch (id){
            case R.id.setting:
                Toast.makeText(this, "Settings Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.about:
                Toast.makeText(this, "About Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.connexion:
            case R.id.connexionTab:
                Intent connexionActivity = new Intent(getApplicationContext(), AuthentificationActivity.class);
                startActivity(connexionActivity);
                break;
            case R.id.getAnImageTab:
                Intent flickrImageActivity = new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(flickrImageActivity);
                break;
            case R.id.getAnImageTabWithPos:
                Intent flickrWithPosActivity = new Intent(getApplicationContext(), SearchWithPosActivity.class);
                startActivity(flickrWithPosActivity);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.exit);
        builder.setMessage(R.string.exit_message);

        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.show();
    }
}